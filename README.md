# LinkedList ([demo link](https://rysev-a.bitbucket.io))

### Main file - [LinkedList](https://bitbucket.org/rysev-a/linked-list/src/master/src/app/LinkedList.js)

### API

- `append` - add iterable at the end
- `reset` - remove all elements
- `reverseImmutable` - get new reversed linkedList
- `reverse` - reverse elements values
- `push` - add element at the start
- `pop` - get last element value and remove from linkedList
- `values` - get all values as array
- `size` - get linkedList size
- `head` - get linkedList head value
