const {
  FuseBox,
  WebIndexPlugin,
  BabelPlugin,
  CSSPlugin,
  QuantumPlugin,
} = require('fuse-box');
const isProduction = process.env.NODE_ENV === 'production';
const fuse = FuseBox.init({
  homeDir: 'src',
  target: 'browser@es6',
  output: 'build/$name.js',
  sourceMaps: true,
  plugins: [
    WebIndexPlugin({
      template: './src/assets/index.html',
    }),
    BabelPlugin({
      config: {
        sourceMaps: true,
        presets: ['env', 'react', 'stage-0'],
        plugins: ['transform-runtime'],
      },
    }),
    CSSPlugin(),
    isProduction && QuantumPlugin(),
  ],
});

const bundle = fuse.bundle('app').instructions(' > app/index.jsx');

if (!isProduction) {
  fuse.dev();
  bundle.hmr().watch('src/**');
}

fuse.run();
