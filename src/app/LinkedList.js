export function LinkedList(iterable = null) {
  this._head = null;
  this._tail = null;
  this.iterable = iterable;

  if (this.iterable) {
    this[Symbol.iterator] = linkedListIterator;
  }
}

LinkedList.prototype.values = function() {
  if (!this._head) {
    return [];
  }

  const getValues = (acc, el) => {
    return el.next
      ? getValues([...acc, el.value], el.next)
      : [...acc, el.value];
  };

  return getValues([], this._head);
};

LinkedList.prototype.size = function() {
  if (!this._head) {
    return 0;
  }

  const getSize = (el, size = 1) => {
    return el.next ? getSize(el.next, size + 1) : size;
  };

  return getSize(this._head);
};

LinkedList.prototype.reset = function() {
  if (!this._head) {
    return false;
  }

  const clearNode = node => {
    if (node.next) {
      clearNode(node.next);
    }

    node.prev = null;
    node.next = null;
    node.value = null;
  };

  clearNode(this._head);

  this._head = null;
  this._tail = null;
};

LinkedList.prototype.append = function(iterable) {
  iterable.map(value => {
    if (this._tail) {
      this._tail.next = new Node(value, null, this._tail);
      this._tail = this._tail.next;
    } else {
      const newNode = new Node(value);
      this._head = newNode;
      this._tail = newNode;
    }
  });
};

LinkedList.prototype.push = function(value) {
  if (this._head) {
    this._head.prev = new Node(value, this._head, null);
    this._head = this._head.prev;
  } else {
    const newNode = new Node(value);
    this._head = newNode;
    this._tail = newNode;
  }
};

LinkedList.prototype.reverse = function() {
  if (this.size() < 2) {
    return false;
  }

  const values = this.values();
  values.reduce((node, value) => {
    node.value = value;
    return node.prev;
  }, this._tail);
};

LinkedList.prototype.reverseImmutable = function() {
  const values = this.values();
  const reversedLinkedList = new LinkedList(this.iterable);
  reversedLinkedList.append(values.reverse());
  return reversedLinkedList;
};

LinkedList.prototype.pop = function() {
  if (this.size() == 1) {
    const value = this._tail.value;
    this._tail.value = null;
    this._tail = null;
    this._head = null;
    return value;
  }

  if (this.size() > 1) {
    const value = this._tail.value;
    this._tail = this._tail.prev;
    this._tail.next = null;
    return value;
  }

  return undefined;
};

LinkedList.prototype.head = function() {
  return this._head && this._head.value;
};

function* linkedListIterator() {
  if (!this._head) {
    return false;
  }

  this.el = this._head;

  while (this.el) {
    yield this.el.value;
    this.el = this.el.next;
  }
}

function Node(value, next, prev) {
  this.value = value;
  this.next = next;
  this.prev = prev;
}
