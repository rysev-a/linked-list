import React, { Component } from 'react';
import { LinkedList } from './LinkedList';
import { Button } from './Button';

export class ListView extends Component {
  constructor(props) {
    super(props);
    this.linkedList = new LinkedList(this.props.iterable);

    // init state
    this.state = {
      head: this.linkedList.head(),
      values: this.linkedList.values(),
      size: this.linkedList.size(),
      linkedList: this.linkedList,
      reveredLinkedList: null,
      popValues: [],
    };
  }

  update = () => {
    this.setState({
      linkedList: this.linkedList,
      head: this.linkedList.head(),
      values: this.linkedList.values(),
      size: this.linkedList.size(),
    });
  };

  append = elements => {
    this.linkedList.append(elements);
    this.update();
  };

  reset = () => {
    this.linkedList.reset();
    this.update();
  };

  push = value => {
    this.linkedList.push(value);
    this.update();
  };

  reverse = () => {
    this.linkedList.reverse();
    this.update();
  };

  reverseImmutable = () => {
    const reveredLinkedList = this.linkedList.reverseImmutable();
    this.setState({ reveredLinkedList });
  };

  pop = () => {
    const popValue = this.linkedList.pop();
    this.update();

    if (popValue) {
      this.setState({ popValues: [...this.state.popValues, popValue] });
    }
  };

  render() {
    return (
      <div className="linked-list">
        <div className="columns">
          <div className="column">
            <h3 className="has-text-weight-normal">Size: {this.state.size}</h3>
            <h3 className="has-text-weight-normal">Values:</h3>
            <div className="tags">
              {this.state.values.map((value, index) => (
                <span className="tag" key={index}>
                  {value}
                </span>
              ))}
            </div>
            <h3 className="has-text-weight-normal">Head: {this.state.head}</h3>

            {this.linkedList.iterable && (
              <div className="iterable">
                <h3 className="has-text-weight-normal">
                  Iterate with [...linkedList]:
                </h3>
                <div className="tags">
                  {[...this.state.linkedList].map((value, index) => (
                    <span className="tag" key={index}>
                      {value}
                    </span>
                  ))}
                </div>
              </div>
            )}
            {this.state.reveredLinkedList && (
              <div className="reversed">
                <br />
                <h3 className="has-text-weight-normal">
                  Immutable reversed linkedList:
                </h3>
                <div className="tags">
                  {this.state.reveredLinkedList.values().map((value, index) => (
                    <span className="tag" key={index}>
                      {value}
                    </span>
                  ))}
                </div>
              </div>
            )}
            {Boolean(this.state.popValues.length) && (
              <div className="pop-values">
                <br />
                <h3 className="has-text-weight-normal">Pop values:</h3>
                <div className="tags">
                  {this.state.popValues.map((value, index) => (
                    <span className="tag" key={index}>
                      {value}
                    </span>
                  ))}
                </div>
              </div>
            )}
          </div>

          <div className="column">
            <Button
              onClick={() => this.append(['appended', 'appended'])}
              title="apend"
            />
            <Button onClick={() => this.reset()} title="reset" />
            <Button
              onClick={() => this.reverseImmutable()}
              title="reverse immutable"
            />
            <Button onClick={() => this.reverse()} title="reverse" />
            <Button onClick={() => this.push('pushed')} title="push" />
            <Button onClick={() => this.pop()} title="pop" />
            <Button
              color="success"
              onClick={() => console.log(this.linkedList)}
              title="console.log list"
            />
          </div>
        </div>
      </div>
    );
  }
}
